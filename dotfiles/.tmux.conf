# set utf8 if using
setw -g utf8 on

# use vi key map on copy mode
# setw -g mode-keys vi
setw -g mode-keys emacs

# change key for escaping tmux control
unbind C-b
set -g prefix C-t

# window
bind-key C-n next-window
bind-key C-p previous-window

# split window
bind-key 2 split-window -h
bind-key 3 split-window -v
bind-key 0 kill-pane

# panel
bind-key C-t last-pane
bind-key C-g swap-pane
bind-key C-h select-pane -L
bind-key C-j select-pane -D
bind-key C-k select-pane -U
bind-key C-l select-pane -R

# toggle statusbar
bind-key b set-option status

# confirm before killing a window or the server
bind-key k confirm kill-window
bind-key K confirm kill-server

# useful
bind-key j new-window
bind-key r command-prompt "rename-window %%"
bind-key R refresh-client
bind-key [ copy-mode
bind-key C-[ copy-mode
bind-key ] paste-buffer
bind-key C-] paste-buffer

# scrollback buffer n lines
set -g history-limit 5000

# listen for activity on all windows
set -g bell-action any

# on-screen time for display-panes in ms
set -g display-panes-time 2000

# start window indexing at one instead of zero
set -g base-index 1

# enable wm window titles
set -g set-titles on

# wm window title string (uses statusbar variables)
set -g set-titles-string "tmux.#I.#W"

# don't change window name automatically
setw -g automatic-rename off

# statusbar --------------------------------------------------------------

set -g display-time 2000
set -g status-interval 1

# terminal color
set -g default-terminal screen-256color

# default statusbar colors
set -g status-fg white
set -g status-bg default
set -g status-attr default

# default window title colors
setw -g window-status-fg cyan
setw -g window-status-bg default
setw -g window-status-attr dim

# active window title colors
setw -g window-status-current-fg white
setw -g window-status-current-bg default
setw -g window-status-current-attr bright,underscore

# command/message line colors
set -g message-fg white
set -g message-bg black
set -g message-attr bright

# center align the window list
set -g status-justify centre

# show some useful stats but only when tmux is started 
# outside of Xorg, otherwise dwm statusbar shows these already
set -g status-left "[#[fg=green] #H #[default]]"
set -g status-right "[ #[fg=magenta]#(cat /proc/loadavg | cut -d \" \" -f 1,2,3)#[default] ][ #[fg=cyan,bright]%a %m-%d %H:%M #[default]]"
set -g status-right-length 50

# tmux-MacOSX-pasteboard
# https://github.com/ChrisJohnsen/tmux-MacOSX-pasteboard/
# make reattach-to-user-namespace && cp reattach-to-user-namespace ~/bin
set -g default-command "reattach-to-user-namespace -l zsh"
bind-key C-c run "tmux save-buffer - | reattach-to-user-namespace pbcopy"
